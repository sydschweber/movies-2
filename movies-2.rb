
#a test class for MovieData
class MovieTest
	@predictions = Array.new
	@actuals = Array.new
	@errors = Array.new

	def initialize(test, real)
		@predictions = test
		@actuals = real
	end

	#calculates the error of the predictions
	def error
		@errors = @predictions.zip(@actuals).map! {|x, y| x - y} #now an array of errors
		@errors.map! {|x| x.abs}
	end

	#calculates the average error value between the predicted values and the actual values
	def mean
		avg = 0
		sum = 0
		@errors.each do |i|
			sum += i.to_i
		end
		avg = sum.to_f/@errors.size.to_f
		return avg
	end

	#calculates the standard deviation of the error
	def stddev
		stddev = 0
		variance = Array.new
		@errors.each do |i|
			variance.push(i - mean)
		end
		variance.map! {|x| (x ** 2)/variance.size}
		stddev = variance.reduce {|x , y| x + y}
		stddev = Math.sqrt(stddev)
		return stddev
	end

	#calculates the root mean square
	def rms
		squares = @errors.map {|x| (x ** 2)}
		rms = (squares.reduce {|x, y| x + y})/@errors.size
		rms = Math.sqrt(rms)
		return rms
	end

	#creates an array of predictions in the form [u,m,r,p]
	def to_a
		
	end
end

#@author Sydney Schweber
#a class that loads movie and user data to determine popularity of a given movie
#and users that give similar reviews

class MovieData
	attr_reader :data_set, :movie_hash, :user_hash, :test_set

	def initialize(data_path, test_path)
		@data_set = File.read(data_path)
		@test_set = File.read(test_path)
		@movie_hash = Hash.new
		@user_hash = Hash.new
	end
	
	#loads the movies data from @data_set
	def load_data
		@data_set.each_line do |line| #for each line in the dataset
			movies = line.split("\t") #split the line by tab
			movie_id = movies[1] #save the user id
			if (!@movie_hash.has_key? movie_id) #if the key does not exist, create a space for it
				@movie_hash.store(movie_id, Array.new {Array.new})
			end
			@movie_hash.store(movie_id, @movie_hash[movie_id].push([movies[0],movies[2]])) #if it exists, add movies that user has rated to array
		end
	end

	#calculates the popularity of a given movie by counting every ranking above 3
	def popularity(movie_id) 
		count = 0
		ratings = Array.new
		movie_hash[movie_id].each do |i|
			ratings.push(i[1])
		end
		ratings.each do |i|
			if(i.to_i > 3)
				count += 1 #the number of ratings a given movie has above 3 stars
			end
		end
		return count 
	end

	#produces a list of movies organized by popularity in descending order
	def popularity_list
		list = movie_hash.keys
		list.sort!{|movie1, movie2| popularity(movie1) <=> popularity(movie2)}
		list.reverse!
		return list
		
	end

	#loads the users into a hash where keys are userIDs and values are 2D arrays with movieID and it's rating from user
	def load_users
		@data_set.each_line do |line| #for each line in the dataset
			users = line.split("\t") #split the line by tab
			user_id = users[0] #save the user id
			if (!@user_hash.has_key? user_id) #if the key does not exist, create a space for it
				@user_hash.store(user_id, Array.new {Array.new})
			end
			@user_hash.store(user_id, @user_hash[user_id].push([users[1],users[2]])) #if it exists, add movies that user has rated to array
		end
	end
	
	#calculates the similarity of two users by taking the size of the intersection between two user values
	def similarity(user1, user2)
		similarity = user_hash[user1] & user_hash[user2] #movies that have been given the same rating by two users
		sim = similarity.size()
		return sim
	end

	#compute similarity of u to all users, keys= userid, values = similarity
	def most_similar(u)
		sim = Hash.new
		list = user_hash.keys #user ids
		list.each do |user|
			if(user != u)
				sim.store(user, similarity(u, user))
			end
		end
		array = sim.sort_by{|_key, value| value} #sort by values (i.e. similarities)
		new_list = Array.new
		i = 0
		array.each do |element|
			new_list.push(array[i][0]) #just push user ids, not similarities
			i += 1
		end
		return new_list.reverse
	end

	#the rating a given user gave a given movie
	def rating(u, m)
		movie_index = getMovieIndex(u, m)
		if (movie_index != nil) #if the user rated that movie
			return @user_hash[u][movie_index][1] ##returns the rating at the given movie index
		else
			return 0
		end
	end

	#returns the index of a given movie in array of values for that user
	def getMovieIndex(u, m)
		rating_list = @user_hash[u]
		movie_index = rating_list.index(rating_list.detect{|movie| movie.include?(m)}) #to get index of movie in 2D array
		return movie_index
	end

	def predict(u, m)
		sim = most_similar(u)
		predicted = 1.0
		movie_index = 0
		sim.each do |user|
			movie_index = getMovieIndex(user, m)
			if (movie_index != nil)
				predicted = @user_hash[user][movie_index][1] #the rating at the given movie index
				return predicted
			end
		end
		return predicted
	end

	#returns an array of movies that a user reviewed
	def movies(u)
		movie_list = Array.new
		@user_hash[u].each do |m| #for each value in the user_hash
			movie_list.push(m[0]) #add the movieID to the array
		end
		return movie_list
	end

	#returns an array of userIDs that reviewed the given movie
	def viewers(m)
		viewer_list = Array.new
		@movie_hash[m].each do |u|
			viewer_list.push(u[0])
		end
		return viewer_list
	end

	def run_test(k)
		predictions = Array.new
		ratings = Array.new
		t = MovieTest.new(predictions, ratings)
		userID = 0
		movieID = 0
		reviews = Array.new
		@test_set.each_line do |line|
			if(k==0)
				return t
			end
			reviews = line.split("\t")
			userID = reviews[0]
			movieID = reviews[1]
			predictions.push(predict(userID, movieID))
			ratings.push(reviews[2])
			ratings.map! {|x| x.to_i}
			predictions.map! {|x| x.to_i}
			k -= 1
		end
	end
	
end
z = MovieData.new('u.data', 'u1.test')
z.load_data
z.load_users
t = z.run_test(5)
t.error
t.mean
t.stddev
t.rms


